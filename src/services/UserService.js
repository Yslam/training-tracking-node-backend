const UserModel = require('../models/User')

module.exports = {
  create(newUser) {
    try {
      const createdUser = new UserModel(newUser)
      return createdUser.save()
    } catch (error) {
      throw { status: 'error', error }
    }
  },

  findByEmail(email) {
    try {
      return UserModel.findOne({ email: email })
    } catch (error) {
      throw { status: 'error', error }
    }
  },

  findByUserName(userName) {
    try {
      return UserModel.findOne({ userName: userName })
    } catch (error) {
      throw { status: 'error', error }
    }
  }
}
