const { check } = require('express-validator')

const validateRegister = [
  check('name')
    .notEmpty({ ignore_whitespace: true })
    .withMessage('Name is required')
    .bail()
    .isAlpha('en-US', { ignore: ' ' })
    .withMessage('Name field use only letters'),
  check('lastName')
    .notEmpty({ ignore_whitespace: true })
    .withMessage('Last name is required')
    .bail()
    .isAlpha('en-US', { ignore: ' ' })
    .withMessage('Last name field use only letters'),
  check('userName')
    .notEmpty({ ignore_whitespace: true })
    .withMessage('User name is required')
    .bail(),
  check('email')
    .notEmpty({ ignore_whitespace: true })
    .withMessage('Email is required')
    .bail()
    .isEmail()
    .withMessage('Please enter a valid email')
    .normalizeEmail(),
  check('role')
    .notEmpty({ ignore_whitespace: true })
    .withMessage('role is required')
    .bail()
    .isIn(['USER', 'ADMIN']),
  check('password')
    .notEmpty({ ignore_whitespace: true })
    .withMessage('Password is required')
    .bail()
    .trim()
    .escape()
]

module.exports = validateRegister
