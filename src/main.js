const express = require('express')
const morgan = require('morgan')
const app = express()

const authenticate = require('./routes/authenticate')
const user = require('./routes/user')

app.use(morgan('dev'))
app.use(express.json())
app.use('/api/authenticate', authenticate)
app.use('/api/user', user)

module.exports = app
