const jwt = require('jsonwebtoken')

const verifyToken = (req, res, next) => {
  const { authorization } = req.headers
  if (!authorization) {
    return res.status(400).json({
      status: 'error',
      message: 'Access denied. Please, provide a Bearer token.'
    })
  }
  try {
    const checkToken = jwt.verify(authorization, process.env.TOKEN_SECRET)
    req.user = checkToken
    next()
  } catch (error) {
    res.status(400).json({
      status: 'error',
      error: 'Access denied. Invalid token'
    })
  }
}

module.exports = verifyToken
