const router = require('express').Router()
const UserService = require('../services/UserService')
const bcrypt = require('bcrypt')
const userRegistrationValidator = require('../validators/registerUser')
const { validationResult } = require('express-validator')

router.post('/register', userRegistrationValidator, async (req, res) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ status: 'error', error: errors.array() })
    }

    const jumps = await bcrypt.genSalt(parseInt(process.env.JUMPS_PASSWORD))
    req.body.password = await bcrypt.hash(req.body.password, jumps)
    UserService.create(req.body)
    res.status(201).json({
      status: true,
      message: 'User created succesful.'
    })
  } catch (error) {
    res.status(500).json({ status: 'error', error })
  }
})
module.exports = router
