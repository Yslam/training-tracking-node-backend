const router = require('express').Router()
const UserService = require('../services/UserService')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

router.post('/', async (req, res) => {
  try {
    const user = await UserService.findByEmail(req.body.email)
    if (user) {
      const authenticatedUser = await bcrypt.compare(
        req.body.password,
        user.password
      )
      if (authenticatedUser) {
        const token = jwt.sign(
          {
            name: user.name,
            id: user.id
          },
          process.env.TOKEN_SECRET
        )
        res.status(200).json({
          status: true,
          token: token,
          message: 'User was authenticated.'
        })
      } else {
        res.status(400).json({
          status: 'error',
          message: 'Invalid credentials.'
        })
      }
    } else {
      res.status(400).json({
        status: 'error',
        message: 'Invalid credentials.'
      })
    }
  } catch (error) {
    res.status(500).json({ status: 'error', error })
  }
})

module.exports = router
