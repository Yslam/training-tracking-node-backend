const mongoose = require('mongoose')
const Schema = mongoose.Schema

const roles = {
  values: ['ADMIN', 'USER'],
  message: '{VALUE} not is a role.'
}

const UserSchema = new Schema({
  name: {
    type: String,
    required: [true],
    minlength: 3,
    maxlength: 255
  },
  lastName: {
    type: String,
    required: [true],
    minlength: 3,
    maxlength: 255
  },
  userName: {
    type: String,
    required: [true],
    index: {
      unique: true
    },
    minlength: 3,
    maxlength: 255
  },
  email: {
    type: String,
    required: [true],
    index: {
      unique: true
    },
    minlength: 6,
    maxlength: 255
  },
  role: {
    type: String,
    default: 'USER',
    required: [true],
    enum: roles
  },
  password: {
    type: String,
    required: [true],
    minlength: 6
  },
  creationDate: String,
  modificationDate: String
})

const UserModel = mongoose.model('user', UserSchema)
UserModel.createIndexes()

module.exports = UserModel
