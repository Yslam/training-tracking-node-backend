const TestUtils = require('./libs/testUtils')
const chaiHttp = require('chai-http')
const app = require('../src/main')
const chai = require('chai')
const UserService = require('../src/services/UserService')

chai.should()
chai.use(chaiHttp)

const API_AUTHENTICATE = '/api/authenticate'

describe('Authenticate', () => {

  const EXAMPLE_USER = { email: 'example@gmail.com', password: 'secretpasword' }

  before(async () => {
    await TestUtils.dbConnect()
    UserService.create(EXAMPLE_USER)
  })

  after(async () => {
    await TestUtils.dbDisconnect()
  })


  describe(API_AUTHENTICATE, () => {
    it.skip('Should get a response', (done) => {
      chai
        .request(app)
        .post(API_AUTHENTICATE)
        .send(EXAMPLE_USER)
        .end((err, res) => {
          console.log('==> ', res.body)
          done()
        })
    })
  })
})
