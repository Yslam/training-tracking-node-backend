const TestUtils = require('./libs/testUtils')
const UserService = require('../src/services/UserService')
const User = require('../src/models/User')
const chaiHttp = require('chai-http')
const app = require('../src/main')
const chai = require('chai')

chai.should()
chai.use(chaiHttp)

const API_USER = '/api/user'
const API_USER_REGISTER = `${API_USER}/register`

describe(API_USER_REGISTER, () => {
  const EXAMPLE_USER = {
    name: 'Maria Betsabe',
    lastName: 'Mora Medina',
    userName: 'MaryBeth',
    email: 'mary@gmail.com',
    role: 'USER',
    password: 'marybeth'
  }

  before(async () => {
    await TestUtils.dbConnect()
    UserService.create(EXAMPLE_USER)
  })

  after(async () => {
    User.deleteMany({})
    await TestUtils.dbDisconnect()
  })

  describe(`Testing the route ${API_USER_REGISTER}`, () => {
    it('Should fail if no data is sent', (done) => {
      chai
        .request(app)
        .post(API_USER_REGISTER)
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('Object')
          res.body.should.have.property('status').eql('error')
          res.body.should.have.property('error')
          res.body.error.should.be.a('Array')
          done()
        })
    })

    it.skip('500 pending review It should fail if it does not connect to the database', (done) => {
      beforeEach(() => {
        TestUtils.dbDisconnect()
      })
      chai
        .request(app)
        .post(API_USER_REGISTER)
        .end((err, res) => {
          res.should.have.status(500)
          res.body.should.be.a('Object')
          res.body.should.have.property('status').eql('error')
          res.body.should.have.property('error')
          res.body.error.should.be.a('Array')
          done()
        })
    })

    it('Should fail for incomplete user', (done) => {
      const incompleteUser = {
        email: 'usertest@mail.com'
      }
      chai
        .request(app)
        .post(API_USER_REGISTER)
        .send(incompleteUser)
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('Object')
          res.body.should.have.property('status').eql('error')
          res.body.should.have.property('error').with.lengthOf(5)
          res.body.error.should.be.a('Array')
          done()
        })
    })

    it('Should fail with wrong fields', (done) => {
      const wrongUser = {
        name: '  ',
        lastName: '1234',
        userName: '  ',
        email: 'mary@.com',
        role: 'user',
        password: ' '
      }
      chai
        .request(app)
        .post(API_USER_REGISTER)
        .send(wrongUser)
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('Object')
          res.body.should.have.property('status').equal('error')
          res.body.should.have.property('error')
          res.body.error[0].should.have
            .property('msg')
            .equal('Name is required')
          res.body.error[1].should.have
            .property('msg')
            .equal('Last name field use only letters')
          res.body.error[2].should.have
            .property('msg')
            .equal('User name is required')
          res.body.error[3].should.have
            .property('msg')
            .equal('Please enter a valid email')
          res.body.error[4].should.have.property('msg').equal('Invalid value')
          res.body.error[5].should.have
            .property('msg')
            .equal('Password is required')
          res.body.error.should.be.a('Array')
          done()
        })
    })

    it.skip('Should fail with existing username', (done) => {
      UserService.create(EXAMPLE_USER)
      chai
        .request(app)
        .post(API_USER_REGISTER)
        .send(EXAMPLE_USER)
        .end((err, res) => {
          res.should.have.status(500)
          res.body.should.be.a('Object')
          res.body.should.have.property('status').eql('error')
          res.body.error.keyValue.should.have
            .property('userName')
            .eql('MaryBeth')
          done()
        })
    })

    it('Should register a user correctly', (done) => {
      const correctUser = { ...EXAMPLE_USER }
      correctUser.userName = 'MaryBethOK'
      correctUser.email = 'MaryBeth@gmail.com'
      chai
        .request(app)
        .post(API_USER_REGISTER)
        .send(correctUser)
        .end((err, res) => {
          res.should.have.status(201)
          res.body.should.be.a('Object')
          res.body.should.have.property('status').eql(true)
          res.body.should.have
            .property('message')
            .eql('User created succesful.')
          res.body.message.should.be.a('String')
          done()
        })
    })
  })
})
